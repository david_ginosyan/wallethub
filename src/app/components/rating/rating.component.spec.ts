import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RatingComponent } from './rating.component';

describe('RatingComponent', () => {
  let component: RatingComponent;
  let fixture: ComponentFixture<RatingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RatingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RatingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render star 3.5', () => {
    component.score = 3.5;
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    const stars = compiled.querySelectorAll('.material-icons');
    expect(stars[0].textContent).toContain('star');
    expect(stars[1].textContent).toContain('star');
    expect(stars[2].textContent).toContain('star');
    expect(stars[3].textContent).toContain('star_half');
    expect(stars[4].textContent).toContain('star_border');
  });

  it('should render star 0', () => {
    component.score = 0;
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    const stars = compiled.querySelectorAll('.material-icons');
    expect(stars[0].textContent).toContain('star_border');
    expect(stars[1].textContent).toContain('star_border');
    expect(stars[2].textContent).toContain('star_border');
    expect(stars[3].textContent).toContain('star_border');
    expect(stars[4].textContent).toContain('star_border');
  });

});

import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';

@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.scss']
})
export class RatingComponent implements OnChanges {

  @Input() score = 0;

  @Output() scoreClickEmitter: EventEmitter<number> = new EventEmitter<number>();

  stars = [...Array(5).keys()];

  constructor() {
  }

  ngOnChanges(): void {

    if (isNaN(this.score)) {
      console.error('Score cannot be string');
      this.score = 0;
    }

  }

  setScore(star: number) {
    this.scoreClickEmitter.next(star);
  }
}

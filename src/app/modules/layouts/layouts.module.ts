import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { LayoutComponent } from './components/layout/layout.component';
import {RatingComponent} from '../../components/rating/rating.component';
import {LayoutsRoutingModule} from './layouts-routing.module';
import {NgContentComponent} from '../../components/ng-content/ng-content.component';



@NgModule({
  declarations: [HeaderComponent, LayoutComponent, RatingComponent, NgContentComponent],
  exports: [
    LayoutComponent,
    RatingComponent,
    NgContentComponent,
  ],
  imports: [
    CommonModule,
    LayoutsRoutingModule,
  ]
})
export class LayoutsModule { }

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LinkOneComponent} from './link-one/link-one.component';
import {LinkTwoComponent} from './link-two/link-two.component';
import {LinkThreeComponent} from './link-three/link-three.component';
import {AuthGuard} from '../../guard/AuthGuard';

const routes: Routes = [
  { path: '', redirectTo: 'link_one', pathMatch: 'full' },
  { path: 'link_one', component: LinkOneComponent, data: {animation: '1'} },
  { path: 'link_two', component: LinkTwoComponent, data: {animation: '0'} },
  { path: 'link_three', component: LinkThreeComponent, data: {animation: '1'} },
  {
    path: 'guard',
    canActivateChild: [AuthGuard],
    canLoad: [AuthGuard],
    component: LinkThreeComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {}

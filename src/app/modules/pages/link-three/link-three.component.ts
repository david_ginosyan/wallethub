import {Component, OnInit, ViewChild} from '@angular/core';
import {DataTransferService} from '../../../services/data-transfer.service';
import {NgContentComponent} from '../../../components/ng-content/ng-content.component';

@Component({
  selector: 'app-link-three',
  templateUrl: './link-three.component.html',
  styleUrls: ['./link-three.component.scss']
})
export class LinkThreeComponent implements OnInit {

  @ViewChild('ngContentComponent', {static: false}) ngContentComponent: NgContentComponent;

  constructor(public dataTransferService: DataTransferService) { }

  ngOnInit() {
  }

  addCounterInChildComponent() {
    this.ngContentComponent.addCounter();
  }

}

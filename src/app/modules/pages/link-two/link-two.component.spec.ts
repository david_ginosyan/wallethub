import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LinkTwoComponent } from './link-two.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

describe('LinkTwoComponent', () => {
  let component: LinkTwoComponent;
  let fixture: ComponentFixture<LinkTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, FormsModule],
      declarations: [ LinkTwoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LinkTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

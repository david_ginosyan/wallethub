import {Component, OnInit} from '@angular/core';
import {DataTransferService} from '../../../services/data-transfer.service';
import {AbstractControl, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators} from '@angular/forms';

@Component({
  selector: 'app-link-two',
  templateUrl: './link-two.component.html',
  styleUrls: ['./link-two.component.scss']
})
export class LinkTwoComponent implements OnInit {

  formGroup: FormGroup;
  isAnimate = false;

  constructor(public dataTransferService: DataTransferService, private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.formGroup = this.formBuilder.group({
      name: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, this.patternValidator(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)])
    });
  }

  patternValidator(regexp: RegExp): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      const value = control.value;
      if (value === '') {
        return null;
      }
      return !regexp.test(value) ? {'patternInvalid': {regexp}} : null;
    };
  }

  animate() {
    this.isAnimate = !this.isAnimate;
  }
}

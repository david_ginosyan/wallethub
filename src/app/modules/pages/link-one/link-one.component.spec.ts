import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LinkOneComponent } from './link-one.component';
import {RatingComponent} from '../../../components/rating/rating.component';

describe('LinkOneComponent', () => {
  let component: LinkOneComponent;
  let fixture: ComponentFixture<LinkOneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LinkOneComponent, RatingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LinkOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render title', () => {
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.title > div').textContent).toContain(' Barclaycard CashForward™ World MasterCard™');
  });
});

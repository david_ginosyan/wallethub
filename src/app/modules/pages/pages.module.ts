import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LinkOneComponent } from './link-one/link-one.component';
import { LinkTwoComponent } from './link-two/link-two.component';
import { LinkThreeComponent } from './link-three/link-three.component';
import {PagesRoutingModule} from './pages-routing.module';
import {LayoutsModule} from '../layouts/layouts.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AppModule} from '../../app.module';



@NgModule({
  declarations: [LinkOneComponent, LinkTwoComponent, LinkThreeComponent],
  imports: [
    CommonModule,
    PagesRoutingModule,
    LayoutsModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class PagesModule { }
